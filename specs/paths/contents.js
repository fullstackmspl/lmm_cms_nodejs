module.exports = [{
    url: "/create",
    post: {
        summary: "create",
        description: "create",
        parameters: [{
            in: "body",
            name: "body",
            description: "create content",
            required: true,
            schema: {
                $ref: "#/definitions/contentCreate"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/lists",
    post: {
        summary: "list content",
        description: "content",
        parameters: [{
            in: "body",
            name: "body",
            description: "Model of user content",
            required: true,
            schema: {
                $ref: "#/definitions/content"
            }
        }],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/getContent/{id}",
    put: {
        summary: "content",
        description: "content",
        parameters: [
            {
                in: "header",
                name: "x-access-token",
                description: "token to access api",
                required: true,
                type: "string"
            },
            {
                in: "path",
                name: "id",
                description: "user id",
                required: true,
                type: "string"
            },

            {
                in: "body",
                name: "body",
                description: "Model of content",
                required: true,
                schema: {
                    $ref: "#/definitions/content"
                }
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

{
    url: "/updatecontent",
    post: {
        summary: "content",
        description: "content",
        parameters: [
            {
                in: "body",
                name: "body",
                description: "Model of content",
                required: true,
                schema: {
                    $ref: "#/definitions/forgotPassword"
                }
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

// {
//     url: "/otpVerifyAndChangePassword",
//     post: {
//         summary: "Verify otp and change Password",
//         description: "erify otp and change Password",
//         parameters: [
//             {
//                 in: "header",
//                 name: "x-access-token",
//                 description: "token to access api",
//                 required: true,
//                 type: "string"
//             },
//             {
//                 in: "body",
//                 name: "body",
//                 description: "Model of verifyOtp",
//                 required: true,
//                 schema: {
//                     $ref: "#/definitions/verifyOtp"
//                 }
//             }
//         ],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },

// {
//     url: "/getUsers",
//     get: {
//         summary: "getUsers",
//         description: "Just hit the api without pass any param",
//         parameters: [
//             {
//                 in: "header",
//                 name: "x-access-token",
//                 description: "token to access api",
//                 required: true,
//                 type: "string"
//             },
//         ],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },

{
    url: "/deletecontent/{id}",
    get: {
        summary: "deletecontent",
        description: "deletecontent",
        parameters: [
            {
                in: "header",
                name: "x-access-token",
                description: "token to access api",
                required: true,
                type: "string"
            },
            {
                in: "path",
                type: "string",
                name: "id",
                description: "content id",
                required: true
            },],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},

// {
//     url: "/delete/{id}",
//     delete: {
//         summary: "delete",
//         description: "delete",
//         parameters: [
//             {
//                 in: "header",
//                 name: "x-access-token",
//                 description: "token to access api",
//                 required: true,
//                 type: "string"
//             },
//             {
//                 in: "path",
//                 type: "string",
//                 name: "id",
//                 description: "user id",
//                 required: true
//             },],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },

// {
//     url: "/update/{id}",
//     put: {
//         summary: "update",
//         description: "update",
//         parameters: [
//             {
//                 in: "header",
//                 name: "x-access-token",
//                 description: "token to access api",
//                 required: true,
//                 type: "string"
//             },
//             {
//                 in: "path",
//                 type: "string",
//                 name: "id",
//                 description: "user id",
//                 required: true
//             },
//             {
//                 in: "body",
//                 name: "body",
//                 description: "Model of user login",
//                 required: true,
//                 schema: {
//                     $ref: "#/definitions/updateUser"
//                 }
//             }
//         ],
//         responses: {
//             default: {
//                 description: "Unexpected error",
//                 schema: {
//                     $ref: "#/definitions/Error"
//                 }
//             }
//         }
//     }
// },
    // {
    //     url: "/uploadProfilePic/{id}",
    //     put: {
    //         summary: "upload Profile Pic ",
    //         description: "upload Profile Pic ",
    //         parameters: [{
    //             in: "formData",
    //             name: "image",
    //             type: "file",
    //             description: "The file to upload.",
    //             required: true,
    //         },
    //         {
    //             in: "path",
    //             type: "string",
    //             name: "id",
    //             description: "user id",
    //             required: true
    //         }
    //         ],
    //         responses: {
    //             default: {
    //                 description: "Unexpected error",
    //                 schema: {
    //                     $ref: "#/definitions/Error"
    //                 }
    //             }
    //         }
    //     }
    // }
];