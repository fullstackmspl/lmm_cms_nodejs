module.exports = [
    {
        name: "campaignCreate",
        properties: {

            subject: {
                type: "string"
            },
            content: {
                type: "string"
            },
            userId: {
                type: "string"
            },
            emails: {
                type: 'array',
                items: {
                    type: "string",
                }
            },
        },
    }
];