module.exports = [
    {
        name: "blogCreate",
        properties: {

            title: {
                type: "string"
            },
            content: {
                type: "string"
            },
            category: {
                type: "string"
            },
            isPublished: {
                type: "string"
            }
        },
    }

];