module.exports = [{
    name: "updateblog",
    properties: {
        title: {
            type: "string"
        },
        content: {
            type: "string"
        },
        category: {
            type: "string"
        },
        isPublished: {
            type: "string"
        }
    }
}];