module.exports = [

    {
        name: "userCreate",
        properties: {

            userName: {
                type: "string"
            },
            email: {
                type: "string"
            },
            password: {
                type: "string"
            },
            deviceToken: {
                type: "string"
            }
        },
    }

];