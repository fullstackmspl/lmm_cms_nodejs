"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const validator = require("../validators");
const path = require("path");
const express = require("express");

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function (req, res) {
        fs.readFile("./public/specs.html", function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });

    app.get("/api/specs", function (req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });
    ////react js project setup////
    // const root = path.join(__dirname, '../../startupbundle_bin_reactjs/', 'build')

    // app.use(express.static(root));

    // app.get('/*', function(req, res, next) {
    //     if (!req.path.includes('api')) {

    //         res.sendFile('index.html', { root });
    //     } else next();
    // });

    app.post(
        "/api/campaigns/create",
        permit.context.validateToken,
        api.campaigns.create
    );

    app.get(
        "/api/campaigns/list",
        permit.context.validateToken,
        api.campaigns.getAll
    );

    app.delete(
        "/api/campaigns/remove/:id",
        permit.context.validateToken,
        api.campaigns.remove
    );

    app.put(
        "/api/campaigns/update/:id",
        permit.context.validateToken,
        api.campaigns.update
    );

    //role api's //

    app.post(
        "/api/roles/create",
        permit.context.builder,
        validator.roles.create,
        api.roles.create
    );

    app.get(
        "/api/roles/getAll",
        permit.context.builder,
        api.roles.getRoles
    );

    app.delete(
        "/api/roles/deleteRole/:id",
        permit.context.validateToken,
        api.roles.deleteRole
    );

    //// images  ////

    app.post(
        '/api/images/uploadSingle',
        permit.context.builder,
        validator.images.upload,
        api.images.uploadSingle
    );

    app.post(
        '/api/images/uploadMultiple',
        permit.context.builder,
        validator.images.upload,
        api.images.uploadMultiple
    );

    app.put(
        '/api/images/remove',
        permit.context.builder,
        api.images.remove
    );

    app.get(
        "/api/images/getFile/:id",
        permit.context.builder,
        api.images.getFile
    );

    /* collection */

    app.post(
        '/api/collections/create',
        permit.context.builder,
        api.collections.create
    );

    app.get(
        "/api/collections/getCollections",
        permit.context.builder,
        api.collections.getCollections
    );

    /* Blogs */

    app.post(
        '/api/blogs/create',
        permit.context.builder,
        api.blogs.create
    );

    app.get(
        "/api/blogs/getBlogs",
        permit.context.builder,
        api.blogs.getBlogs
    );

    app.get(
        "/api/blogs/getBlog/:id",
        permit.context.builder,
        api.blogs.getBlog
    );

    app.delete(
        "/api/blogs/delete/:id",
        permit.context.builder,
        api.blogs.deleteBlog
    );

    app.put(
        "/api/blogs/update/:id",
        permit.context.builder,
        api.blogs.update
    );

    app.post(
        '/api/blogs/setAboutUs',
        permit.context.builder,
        api.blogs.setAboutUs
    );

    app.put(
        "/api/blogs/blogStatus/:id",
        permit.context.builder,
        api.blogs.blogStatus
    );

    /* FAQS */

    app.post(
        '/api/faqs/create',
        permit.context.builder,
        api.faqs.create
    );

    app.get(
        "/api/faqs/getFaqs",
        permit.context.builder,
        api.faqs.getFaqs
    );

    app.get(
        "/api/faqs/getFaq/:id",
        permit.context.builder,
        api.faqs.getFaq
    );

    app.delete(
        "/api/faqs/delete/:id",
        permit.context.builder,
        api.faqs.deleteFaq
    );

    app.put(
        "/api/faqs/update/:id",
        permit.context.builder,
        api.faqs.update
    );

    log.end();
};

exports.configure = configure;