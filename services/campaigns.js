var nodemailer = require('nodemailer')
const setCampaign = async (model, campaign, context) => {
    const log = context.logger.start("services:campaigns:set");
    if (model.subject !== "string" && model.subject !== undefined) {
        campaign.subject = model.subject;
    }

    if (model.content !== "string" && model.content !== undefined) {
        campaign.content = model.content;
    }

    if (model.emails.length > 0) {
        campaign.emails = model.emails;
    }
    log.end();
    await campaign.save();
    return campaign;

};

const buildCampaign = async (model, context) => {
    const { subject, content, emails, userId } = model;
    const log = context.logger.start(`services:campaigns:buildCampaign${model}`);
    const campaign = await new db.campaign({
        subject: subject,
        content: content,
        emails: emails,
        userId: userId,
        createdOn: new Date(),
        updateOn: new Date()
    }).save();
    log.end();
    return campaign;
};

const create = async (model, context) => {
    const log = context.logger.start("services:campaigns:create");
    const campaign = buildCampaign(model, context);
    log.end();
    return campaign;
};


// getUsers
const getCampaigns = async (query, context) => {
    const log = context.logger.start(`services:campaigns:getCampaigns`);
    const campaigns = await db.campaign.find()
    log.end();
    return campaigns;
};


const deleteCampaign = async (id, context) => {
    const log = context.logger.start(`services:campaigns:deleteUser`);
    if (!id) {
        throw new Error("id is requried");
    }
    let campaign = await db.campaign.deleteOne({ _id: id });

    if (campaign.deletedCount <= 0) {
        throw new Error("something went wrong");
    }

    return 'campaign Deleted Successfully'
};

const update = async (id, model, context) => {
    const log = context.logger.start(`services:campaigns:update`);
    let entity = await db.campaign.findById(id)
    if (!entity) {
        throw new Error("invalid campaign id");
    }
    const campaign = await setCampaign(model, entity, context);
    log.end();
    return campaign
};


exports.create = create;
exports.getCampaigns = getCampaigns;
exports.deleteCampaign = deleteCampaign;
exports.update = update;