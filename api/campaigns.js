"use strict";
const service = require("../services/campaigns");
const response = require("../exchange/response");

const create = async (req, res) => {
    const log = req.context.logger.start(`api:campaigns:create`);
    try {
        const campaign = await service.create(req.body, req.context);
        const message = "campaign created Successfully";
        log.end();
        return response.success(res, message, campaign);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


const getAll = async (req, res) => {
    const log = req.context.logger.start(`api:campaigns:getAll`);
    try {
        const campaign = await service.getCampaigns(req.body, req.context);
        const message = "campaign get Successfully";
        log.end();
        return response.success(res, message, campaign);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const remove = async (req, res) => {
    const log = req.context.logger.start(`api:campaigns:remove`);
    try {
        const campaign = await service.deleteCampaign(req.params.id, req.context);
        log.end();
        return response.data(res, campaign);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async (req, res) => {
    const log = req.context.logger.start(`api:campaigns:update`);
    try {
        const campaign = await service.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, campaign);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};


exports.create = create;
exports.getAll = getAll;
exports.remove = remove;
exports.update = update;