"use strict";
const mongoose = require("mongoose");
const campaign = mongoose.Schema({
    subject: { type: String, required: true },
    content: { type: String, required: true },
    userId: {
        type: mongoose.Schema.Types.ObjectId, required: true,
        refPath: 'user', required: true,
    },
    emails: [{ type: String, required: true, trim: true }],
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});
mongoose.model("campaign", campaign);
module.exports = campaign;